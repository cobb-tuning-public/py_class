Python Class 01
=======
##Install Links
[Python 2.7.15](https://www.python.org/downloads/release/python-2715/)

[Python 3.7.2](https://www.python.org/downloads/release/python-372/)

######
[PyCharm Community Edition](https://www.jetbrains.com/pycharm/download/#section=windows)

[PyCharm Video Guides](https://www.jetbrains.com/pycharm/documentation/)
##
#####PyCharm Env
COBB Pycharm Settings
```
<code_scheme name="COBB_Python">
  <option name="LINE_SEPARATOR" value="
" />
  <option name="ALIGN_MULTILINE_PARAMETERS_IN_CALLS" value="true" />
  <Python>
    <option name="DICT_ALIGNMENT" value="2" />
    <option name="OPTIMIZE_IMPORTS_SORT_NAMES_IN_FROM_IMPORTS" value="true" />
    <option name="FROM_IMPORT_PARENTHESES_FORCE_IF_MULTILINE" value="true" />
  </Python>
  <codeStyleSettings language="Python">
    <indentOptions>
      <option name="INDENT_SIZE" value="2" />
      <option name="CONTINUATION_INDENT_SIZE" value="2" />
      <option name="TAB_SIZE" value="2" />
    </indentOptions>
  </codeStyleSettings>
</code_scheme>
```
##
#####Command Line
You can open a command prompt/powershell/bash and type python. This will start the python dev console. You can exit this console with the exit() command. 
##
#####Tune the World
The simplest of python scripts simply prints to an output a string.

```python
print ("Tune the World")
```

* Single quote vs Double quotes
```python
print ("Tune the World")
print ('Tune the World')
```


* Combine Strings
```python
print ("Tune the" + "World")
```


* Illegal strings
```python
print ('Tune the" + "World")
print ('Can't Do This')
print (Broke)
```


* Escape Chars
```python
print ('You\'ll have to do this')
```


#
#####Variables
* Make your string a variable
```python
var = "Tune the World"
print (var)
```


* Do operations on init
```python
var = 20/4
print (var)
```


* Change the variable and print again
```python
var = "Tune the World"
print (var)
var = 20/4
print (var)
```


#
#####Math
* Add 4+2
* Subtract 10-1
* Multiply 2*2
* Divide 200/2 
* Exponents 4**4
* Order of operation
  * P   Parentheses, then
  * E   Exponents, then
  * MD  Multiplication and division, left to right, then
  * AS  Addition and subtraction, left to right
#####Types
* String
```python
print (type('Taco'))
```


* Integer
```python
print (type(10))
```


* Float
```python
print (type(3.8))
```


* Complex
```python
print (type(4+1j))
```


* Boolean
```python
print (type(True))
```


* None
```python
print (type(None))
```


* List
```python
print (type(['Taco', 5, True, None]))
```


* Dictionary
```python
print (type({'Taco':5, 'Burrito':2, 'Quesadilla':20}))
```


* Tuple
```python
var = 1, 'Taco', False
print (type(var))
```

