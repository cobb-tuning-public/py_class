Python Class 6
=======

#####Logging
The print statement is great in python, it makes debugging really easy, but in larger programs it becomes cumbersome. There is a logging module that takes it to the next level, it allows you to curate your log messages, and also pipe them to different streams or files.


```python
import logging
logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

log.info("Hello Class")

```


##
#####Logging Levels
There are five standard logging levels, but you can configure up to 0-50 if needed. At whatever level you set, logging will display all levels above that level but not below.

* CRITICAL 50 Highest level
* ERROR 40
* WARNING 30
* INFO 20
* DEBUG 10
* NO_LEVEL 0 Lowest level or not set

```python
import logging
logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

log.info("Hello Class")
log.debug("This message will only display if the logging level is set to DEBUG")
log.critical("This message will always display")

```

##
##### Multiple loggers
You can selectively turn on loggers within functions or files. This is hierachical, any functions called by this function iherit this logger, when it returns the logger is restored to the callee's level.
```python
def get_missing_params(list, master_list):
    log = logging.getLogger(__name__+'.get_missing_params')
    log.setLevel(logging.DEBUG)
    list = [x for x in master_list if x not in list]
    log.debug(list)
    return list

```


##
#####Logging Format
You can configure your log message as well. There are a number of keywords you can use to add information to your log.
* asctime     | %(asctime)s     | Human-readable date/time
* levelname   | %(levelname)s   | Logging level string
* filename    | %(filename)s    | Filename of current module
* funcName    | %(funcName)s    | Name of current function
* lineno      | %(lineno)d      | Line number of logging call
* module      | %(module)s      | Module name (without .py)
* message     | %(message)s     | Log Message
* name        | %(name)s        | Logger instance name
* process     | %(process)d     | Process ID
* processName | %(processName)s | Process Name
* thread      | %(thread)d      | Thread ID
* threadName  | %(threadName)s  | Thread Name


```python
import logging
logging.basicConfig(level=logging.INFO, format='%(levelname)2s: %(name)2s: %(funcName)2s: %(message)s')
log = logging.getLogger(__name__)
log.info("Test")
```

##
##### Log To A File or Streams
You can also configure the logger to output to a file. This will stop the logging to the console and put that log in a file. But you can also add extra handlers to your logging instance if you want both outputs. They can also be at different levels.





```python
import logging
import sys

log = logging.getLogger(__name__)
lf = 'py.log'
fh = logging.FileHandler(lf)
format = logging.Formatter('%(levelname)2s: %(name)2s: %(funcName)2s: %(message)s')
fh.setFormatter(format)
log.addHandler(fh)
stdh = logging.StreamHandler(sys.stdout)
stdh.setFormatter(format)
stdh.setLevel(logging.WARNING)
log.addHandler(stdh)
log.setLevel(logging.DEBUG)

log.debug('Test Debug Out')
log.warn('This is a Drill')


```


##
##### Decorators
You can use decorators to add external logging to functions. This is helpful if you need to log similarly for a number of modules.
```python
import logging
import time
import functools

def logging_decorator(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        logging.debug('Entering %s', f.__name__)
        start = time.time()
        f(*args, **kwargs)
        end = time.time()
        logging.debug('Exiting %s in %-5.2f secs', f.__name__, end-start)

    return wrapper

@logging_decorator
def do_work(timeout):
    # Doing something expensive
    time.sleep(timeout)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    do_work(5)
```


##
##### subprocess
The subprocess module is used to spawn new processes and connect their input/output pipes and return codes into python.
There are a couple different ways to use subprocess between python 2.7 and 3.7 but they all go back the the Popen class. The important arguments are listed below.
*args | the arguments you want to pass to the console in a list
* cwd | the working directory 
* stdout | what will handle this stream (HINT: use PIPE)
* stderr | what will handle this stream
* Special value that can be used as the stdin, stdout or stderr argument to Popen and indicates that a pipe to the standard stream should be opened.
* shell | if the command will be executed by the shell
```python
import logging
import subprocess
import os
log = logging.getLogger(__name__)
cmd= ['ls', '-l']
proc = subprocess.Popen(args=command, cwd=os.getcwd(),
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout_data, stderr_data = proc.communicate()
if proc.returncode != 0:
    log.info(proc.returncode)
    log.info(stdout_data)
    log.error(stderr_data)
    exit()
```


##
##### Replacing old shell commands with subprocess
* For trusted input, the shell’s own pipeline support may still be used directly:
```
output=`dmesg | grep hda`
```
Becomes
```python
from subprocess import *
p1 = Popen(["dmesg"], stdout=PIPE)
p2 = Popen(["grep", "hda"], stdin=p1.stdout, stdout=PIPE)
p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
output = p2.communicate()[0]
```
```
output=`mycmd myarg`
```
Becomes
```python
output = check_output(["mycmd", "myarg"])
```


##
##### Subprocess Exceptions
Exceptions raised in the child process, before the new program has started to execute, will be re-raised in the parent. Additionally, the exception object will have one extra attribute called child_traceback, which is a string containing traceback information from the child’s point of view.

The most common exception raised is OSError. This occurs, for example, when trying to execute a non-existent file. Applications should prepare for OSError exceptions.


```python
import logging
import subprocess
import os
log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

command= ['cpss']
try:
    proc = subprocess.Popen(args=command, cwd=os.getcwd(),
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_data, stderr_data = proc.communicate()
    if proc.returncode != 0:
        log.info(proc.returncode)
        log.info(stdout_data)
        log.error(stderr_data)
        exit()
    log.info(stdout_data)
except OSError as e:
    log.error(e)

```
##
##### Homework:
* https://docs.python.org/2.7/library/subprocess.html
* https://docs.python.org/3.7/library/logging.html

This week we are going to build on homework 5 so if you have not finished it do so. 
* Add logging to homework 5, no print statements remain
* log.debug the list of files found 
* log.warn if the default log list is not in a file
* log.info the start and end of your functions
* use a file to log every level
* use stdout to show warnings and above
* use subprocess to make two folders called, good and bad
* Copy the logs with all the default log list into 'good'
* Finally any files that are missing the default log list copy to 'bad'

USE SUBPROCESS NOT OS TO DO THIS FILE OPERATION