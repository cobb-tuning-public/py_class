Python Class 9  
=======

#####Packages
Packages are a way to organize modules in a "." namespace. Packages are simply just a collection of modules. 

```
sound/                          Top-level package
      __init__.py               Initialize the sound package
      formats/                  Subpackage for file format conversions
              __init__.py
              wavread.py
              wavwrite.py
              aiffread.py
              aiffwrite.py
              auread.py
              auwrite.py
              ...
      effects/                  Subpackage for sound effects
              __init__.py
              echo.py
              surround.py
              reverse.py
              ...
      filters/                  Subpackage for filters
              __init__.py
              equalizer.py
              vocoder.py
              karaoke.py
              ...
```


##
##### Init
The __init__.py files are required to make Python treat directories containing the file as packages. This prevents directories with a common name, such as string, unintentionally hiding valid modules that occur later on the module search path. In the simplest case, __init__.py can just be an empty file
```python
__init__.py contents...
__all__ = ["echo", "surround", "reverse"]
```
##
##### Importing from packages
You can import from packages in a number of different ways. The "." will move you into different sub-directories

```python
import sound.effects.echo

sound.effects.echo.echofilter(input, output, delay=0.7, atten=4)

from sound.effects import echo

echo.echofilter(input, output, delay=0.7, atten=4)

from sound.effects.echo import echofilter

echofilter(input, output, delay=0.7, atten=4)


```


##
##### Importing *
Importing an entire package could potentially take a long amount of time so it is not allowed. The only solution is for the package author to provide an explicit index of the package. The import statement uses the following convention: if a package’s __init__.py code defines a list named __all__, it is taken to be the list of module names that should be imported when from package import * is encountered. It is up to the package author to keep this list up-to-date when a new version of the package is released. 


```python
from sound.effects import *

__init__.py contents...
__all__ = ["echo", "surround", "reverse"]

```

##
##### Intra package reference
When packages are structured into subpackages (as with the sound package in the example), you can use absolute imports to refer to submodules of siblings packages. For example, if the module sound.filters.vocoder needs to use the echo module in the sound.effects package, it can use from sound.effects import echo.

You can also write relative imports, with the from module import name form of import statement. These imports use leading dots to indicate the current and parent packages involved in the relative import. From the surround module for example, you might use:
```python
from . import echo
from .. import formats
from ..filters import equalizer
```



##
##### Homework:
GO HAVE FUN!
```python

```