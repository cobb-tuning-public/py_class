Python Class 02
=======

#####Comparison Operators
Comparisons yield boolean values: either True or False.
* \>=
* \<=
* \>
* \< 
* ==
* !=
* is
* is not
* is in


```python
x = 10
y = 12

# Output: x > y is False
print('x > y  is',x>y)

# Output: x < y is True
print('x < y  is',x<y)

# Output: x == y is False
print('x == y is',x==y)

# Output: x != y is True
print('x != y is',x!=y)

# Output: x >= y is False
print('x >= y is',x>=y)

# Output: x <= y is True
print('x <= y is',x<=y)
```


##
#####Logical Operators
* and (Both have to be True)
* or (One or the other has to be True)
* not (Inverts any output)
```python
x = True
y = False

# Output: x and y is False
print('x and y is',x and y)

# Output: x or y is True
print('x or y is',x or y)

# Output: not x is False
print('not x is',not x)
```

##
#####Identity Operators
They are used to check if two values (or variables) are located on the same part of the memory. Two variables that are equal does not imply that they are identical.
* is
* is not


```python
x1 = 5
y1 = 5
x2 = 'Hello'
y2 = 'Hello'
x3 = [1,2,3]
y3 = [1,2,3]

# Output: False
print(x1 is not y1)

# Output: True
print(x2 is y2)

# Output: False
print(x3 is y3)
```


##
#####Membership Operators
They are used to test whether a value or variable is found in a sequence. List, dictionary, string, tuple, set.
* in
* not in


```python
x = 'Hello world'
y = {1:'a',2:'b'}

# Output: True
print('H' in x)

# Output: True
print('hello' not in x)

# Output: True
print(1 in y)

# Output: False
print('a' in y)
```

##
##### Case/Loop/Function Syntax
* For any case/loop or function the statement is written on the same indentation as the code above and
followed by a colon ":", any code indented below this statement is part of that block.



```python
if True:
    print ("This is code inside the if statement")
print ("This code is outside the if statement")

for i in range(0,100):
    print ("This code is inside the For loop")
    if i == 3:
        print ("This code is inside the forloop and the if statement")
print ("This code is outside the for loop and the if statement")
```


##
##### If, elseif, and else
* if
* elif
* else


```python
x = 28

if x < 0:
    # executes only if x < 0
    print('x < 0')                      
elif x == 0:
    # if it's not true that x < 0, check if x == 0
    print('x is zero')
elif x == 1:
    # if it's not true that x < 0 and x != 0, check if x == 1
    print('x == 1')
else:
    print('non of the above is true')
```

##
##### For Loops
* for x in iterable thing



```python
hello_world = "Hello, World!"

for ch in hello_world:    # print each character from hello_world
    print(ch)

# for each number i in range 0-4. range(5) function returns list [0, 1, 2, 3, 4]
for i in range(5):
    # this line is executed 5 times. First time i equals 0, then 1, ...
    print(i)          
```

##
##### While Loops
* while


```python
square = 1

while square <= 10:
    print(square)    # This code is executed 10 times
    square += 1      # This code is executed 10 times
```

##
##### Continue
* The continue keyword allows your to skip the rest of a loop's operation but still remain in the loop.


```python
for i in range(5):
    if i == 3:
        # skip the rest of the code inside loop for current i value
        continue
    print(i)
```

##
##### Break
* The break keyword is used to exit the current loop.


```python
count = 0

while True:  # this condition cannot possibly be false
    print(count)
    count += 1
    if count >= 5:
        break           # exit loop if count >= 5
```

##
##### Pass
* The pass keyword is used when you don't want anything to happen. It can be used a placeholder. Similar to a 'nop'.


```python
for i in range(5):
    if i == 3:
        pass
    else:
      print(i)
```


##
##### Homework
* Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.


```python
for i in range(5):
    if i == 3:
        pass
    else:
      print(i)
```