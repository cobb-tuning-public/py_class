Python Class 04
=======

#####File Operations
You can use the Open command to open a file for reading/writing/appending.
 * 'r'   Open text file for reading.  The stream is positioned at the
         beginning of the file.

 * 'r+'  Open for reading and writing.  The stream is positioned at the
         beginning of the file.

 * 'w'   Truncate file to zero length or create text file for writing.
         The stream is positioned at the beginning of the file.

 * 'w+'   Open for reading and writing.  The file is created if it does not
         exist, otherwise it is truncated.  The stream is positioned at
         the beginning of the file.

 * 'a'   Open for writing.  The file is created if it does not exist.  The
         stream is positioned at the end of the file.  Subsequent writes
         to the file will always end up at the then current end of file,
         irrespective of any intervening fseek(3) or similar.

 * 'a+'  Open for reading and writing.  The file is created if it does not
         exist.  The stream is positioned at the end of the file.  Subse-
         quent writes to the file will always end up at the then current
         end of file, irrespective of any intervening fseek(3) or similar.
 * 'b'   Open the file as a binary file without formatting.


```python
f = open("py_class_04_countries.txt","r")
data = f.readlines()
f.close()
```


##
#####With and Try Execpt
There is a safe way to open files in python, that will catch any errors and handle all file closing. To do this use a Try/Except block along with a with statement.
```python
try:
    with open("py_class_04_countries.txt", "r") as f:
      data = f.readlines()
except IOError as e:
    print (e)
    exit()

```

##
#####Working With Files
You can read data from files in multiple ways, you can use "seek" and "read" to manually move around a file or you can use readline or readlines to read lines in a text files.


```python
# read entire file and put each line in a list
data=[]
try:
    with open("py_class_04_countries.txt", "r") as f:
      for line in f:
        data.append(f.readline())
except IOError as e:
    print (e)
    exit()

# read entire file as a string
try:
    with open("py_class_04_countries.txt", "r") as f:
      data = f.read()
except IOError as e:
    print (e)
    exit()

# read entire file as a string
try:
    with open("py_class_04_countries.txt", "r") as f:
      data = f.seek(-10,2)
except IOError as e:
    print (e)
    exit()
```


##
#####Seeking
Seeking is a way to move around the file using offsets, most important thing to remember is from_where.
* f.seek(offset, from_what) defaults to 0
* 0: means from the start of the file
* 1: means from the current position
* 2: means from the end of the file


To find the current position in the file use f.tell()

```python
# read entire file as a string
try:
    with open("py_class_04_countries.txt", "rb") as f:
      # Move 10 characters back from the end of the file
      f.seek(-10,2)
      # Read 1 byte, this will move the offset by one since we are reading as a text file
      f.read(1)
      
except IOError as e:
    print (e)
    exit()
```

##
##### Writing to a file
* For any case/loop or function the statement is written on the same indentation as the code above and
followed by a colon ":", any code indented below this statement is part of that block.



```python
planets = ['Mercury\n',
'Venus\n',
'Earth\n',
'Mars\n',
'Jupiter\n',
'Saturn\n',
'Uranus\n',
'Neptune\n']
try:
    with open("py_class_04_output.txt", "w") as f:
        # for planet in planets:
        #     f.(planet)
        f.writelines(planets)

except IOError as e:
    print (e)
    exit()
```


##
##### Modifying Files
You can open up a file for reading and writing and modify the contents and re-save it.


```python
try:
    with open("py_class_04_EU.txt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            if "United Kingdom" not in line:
                f.write(line)

except IOError as e:
    print (e)
    exit()
```


##
##### Homework
* Write a program that will create a file with a list of countries that are NOT in EU, use "py_class_04_EU.txt" and "py_class_04_countries.txt" as inputs.


```python
try:
    with open("file1", "r") as f1, open("file2", "r") as f2:
      d1 = f1.readlines()
      d2 = f2.readlines()
except IOError as e:
    print (e)
    exit()
```

##
##### Bonus Homework:
Write a program to parse the file python_class_04_alice_in_wonderland.txt and output a file that contains the count of each Alphabetical A-Z character within it, count lower and uppercase as the same.
The format should be "Letter: Count" e.x "A : 420". Output file should be called "alice_in_wonderland_summary.txt".


```
Alice in Wonderland Summary:
Total Lines : 13000
Total Words : 1455
A : 420
B : 785
C : 5
...
```