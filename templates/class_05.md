Python Class 5
=======

#####List Comprehension
List comprehension provides a concise way to create lists.


```python
new_list = []
for i in old_list:
    if filter(i):
        new_list.append(expressions(i))
# To this 
new_list = [expression(i) for i in old_list if filter(i)]
```


##
#####Basic List Comprehension
In basic list comprehension we want to modify the existing list and create a new one.
```python
manufacturer = ['Volkswagen', 'Subaru', 'Porsche', 'Nissan', 'Mazda', 'Ford', 'BMW', 'Mitsubishi**']

manufacturer_lower = [m.lower() for m in manufacturer]
manufacturer_uppper = [m.upper() for m in manufacturer]

```

##
##### Conditional List Comprehension
You can filter as well as modify the incoming list.

```python
dists = ['AP-VLK-001',
         'AP-VLK-002',
         'AP-VLK-003',
         'AP-POR-001',
         'AP-POR-002',
         'AP-POR-003',
         'AP-POR-004',
         'AP-POR-005',
         'AP-POR-006',
         'AP-POR-007',
         'AP-POR-008',
         'AP-POR-009',
         'AP-POR-010',
         'AP-POR-011',
         'AP-POR-012',
         'AP-FOR-001',
         'AP-FOR-002',
         'AP-FOR-003',
         'AP-FOR-004',
         'AP-FOR-005',
         ]
def GetDist(dist, distlist=dists):
    return [d for d in distlist if dist in d]

print (GetDist('POR'))
print (GetDist('002'))

```


##
#####FizzBuzz List comprehension
```python
fblist = ["Fizz"*(not i%3) + "Buzz"*(not i%5) or i for i in range(1, 101)]
```

##
##### Countries Not In EU




```python
with open("py_class_04_countries.txt","r")as c, open("py_class_04_EU.txt","r") as e, open("py_class_04_not_eu_countries.txt","w")as n:
    countries = c.readlines()
    eu = e.readlines()
    not_eu_countries = [x for x in countries if x not in eu]
    n.writelines(not_eu_countries)
```


##
##### Dictionaries
A dictionary is similar to a list, except that you access its values by looking up a key instead of an index. A key can be any string or a number. Dictionaries are enclosed in curly braces e.g. dct = {'key1' : "value1", 'key2' : "value2"}. 

```python
# create new dictionary.
phone_book = {"John": 123, "Jane": 234, "Jerard": 345}    # "John", "Jane" and "Jerard" are keys and numbers are values
print(phone_book)

# Add new item to the dictionary
phone_book["Jill"] = 345
print(phone_book)

# Remove key-value pair from phone_book
del phone_book['John']

print(Jane's phone)
```


##
##### Get Values from the dictionary
There are a number of methods to get information out of a dictionary.

* dict.keys() returns a list of all the keys in the dictionary
* dict.values() returns a list of all the values
* dict[key] will return the value of that key but this is dangerous because the key might not exist UNSAFE
* dict.get(key,default condition) can be used to check if a value exists without generating a key error
* You can use "in" or "not in" to evaluate if key exists as well
* dict.items{} will return a tuple list of the contents of the dict

```python
dists = {'Volkswagen':
         ['AP-VLK-001',
         'AP-VLK-002',
         'AP-VLK-003'],
         'Porsche':
         ['AP-POR-001',
         'AP-POR-002',
         'AP-POR-003',
         'AP-POR-004',
         'AP-POR-005',
         'AP-POR-006',
         'AP-POR-007',
         'AP-POR-008',
         'AP-POR-009',
         'AP-POR-010',
         'AP-POR-011',
         'AP-POR-012'],
         'Ford':
         ['AP-FOR-001',
         'AP-FOR-002',
         'AP-FOR-003',
         'AP-FOR-004',
         'AP-FOR-005']}
         
print (dists.values())
print (dists.keys())
print dists.get('Mazda', False)
```


##
##### Iterating over dictionaries
There are a number of methods iterate over dictionaries.
```python
dists = {'Volkswagen':
         ['AP-VLK-001',
         'AP-VLK-002',
         'AP-VLK-003'],
         'Porsche':
         ['AP-POR-001',
         'AP-POR-002',
         'AP-POR-003',
         'AP-POR-004',
         'AP-POR-005',
         'AP-POR-006',
         'AP-POR-007',
         'AP-POR-008',
         'AP-POR-009',
         'AP-POR-010',
         'AP-POR-011',
         'AP-POR-012'],
         'Ford':
         ['AP-FOR-001',
         'AP-FOR-002',
         'AP-FOR-003',
         'AP-FOR-004',
         'AP-FOR-005']}
         
for key in dists:
    print (dists.get(key))

for key in dists.iterkeys():
    pass

for val in dists.itervalues():
    pass

```


##
##### Dictionary Modification
You can add to a dictionary, delete a value, pop a key value pair, merge two dictionaries.
* dict.update(other dictionary)
* dict.pop(key, default)
* dict.clear() clears the dictionary
* del dict[key] UNSAFE USE POP
```python
dvw = {'Volkswagen':
           ['AP-VLK-001',
            'AP-VLK-002',
            'AP-VLK-003']}
dp = {'Porsche':
          ['AP-POR-001',
           'AP-POR-002',
           'AP-POR-003',
           'AP-POR-004',
           'AP-POR-005',
           'AP-POR-006',
           'AP-POR-007',
           'AP-POR-008',
           'AP-POR-009',
           'AP-POR-010',
           'AP-POR-011',
           'AP-POR-012'],
      }

df = {'Ford': ['AP-FOR-001',
               'AP-FOR-002',
               'AP-FOR-003',
               'AP-FOR-004',
               'AP-FOR-005']}

dists = {}

dists.update(dvw)
print dists
dists.update(dp)
print dists
dists.pop('Porsche')
print dists

```


##
##### Dictionary Comprehension
You can use all the fun list comprehension the same way to make a dictionary

```python
dict_of_doubles = {
                   str(k) : 2*k for k in range(100)
                  }

dict_of_multiples_of_4 = {
                   str(k) : 2*k for k in range(100) if k%2==0
                  }

```
##
##### Homework:
Tuners always send in log files that don't include the monitors I want, I have a list of standard monitors below, make a program to check if they have the default log list in the CSV files. Use the template below to get you started.
* https://docs.python.org/3/library/csv.html
* https://docs.python.org/3/library/os.html
* https://docs.python.org/3/library/glob.html

BONUS:
Have the program tell me at what time and in what logs knock counts go below -2 on 'Knock Retard Cylinder 4 (Degrees)', 'Knock Retard Cylinder 3 (Degrees)','Knock Retard Cylinder 2 (Degrees)','Knock Retard Cylinder 1 (Degrees)'


```python
import glob
import os
import csv

default_log_list= ['Engine Speed (RPM)',
                   'Time (sec)',
                   'Knock Retard Cylinder 4 (Degrees)',
                   'Knock Retard Cylinder 3 (Degrees)'
                   'Knock Retard Cylinder 2 (Degrees)'
                   'Knock Retard Cylinder 1 (Degrees)',
                   'Ignition Timing Final (Degrees)'
                   'Trgt. Boost Press. (psi)',
                   'Longitudinal Acceleration (m/s2)']


def get_list_of_files(path, filter):
    # Marvel at this list comprehension
    file_list = [y for x in os.walk(path) for y in glob.glob(os.path.join(x[0], filter))]
    return file_list


def main():
    csv_files = get_list_of_files(os.path.join(os.getcwd(),'py_class_05'),'*.csv')
    for file in csv_files:
        try:
            with open(file) as csvfile:
                cf = csv.DictReader(csvfile)
                ''' This is where you would want to do your work, perhaps a function...'''
        except IOError as e:
            print (e)
            exit()


if __name__ == '__main__':
    main()

```