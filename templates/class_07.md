Python Class 7
=======

#####PYTEST
https://docs.pytest.org/en/latest/


The pytest framework makes it easy to write small tests, yet scales to support complex functional testing for applications and libraries.
For this exercise you will need a couple of packages
* pytest
* pytest-tempdir
* pytest-datadir


```python
PS C:\dev\root\prod_scripts\pyprod> pytest
============================= test session starts =============================
platform win32 -- Python 2.7.15, pytest-3.9.2, py-1.7.0, pluggy-0.8.0
rootdir: C:\dev\root\prod_scripts\pyprod, inifile:
plugins: datadir-1.2.1
collected 3 items

tests\test_cfg.py ...                                                    [100%]

========================== 3 passed in 0.20 seconds ===========================
PS C:\dev\root\prod_scripts\pyprod>

```


##
##### 'test' keyword 'tests' Directory
If no arguments are passed to pytest it will recursively search for test_*.py or *_test.py files. From those files it will collect items with...
* test prefixed test functions or methods outside of class
* test prefixed test functions or methods inside Test prefixed test classes (without an __init__ method)
* If you are using a testing folder __init__.py files need to be used to make a package
```python
# test_inc.py
def test_inc():
    assert inc(3) == 5

```

##
##### Test Fixtures
The purpose of test fixtures is to provide a fixed baseline upon which tests can reliably and repeatedly execute.
To use a test fixture you simply pass that function to your test function.
* You can also scope fixtures, scopes are: function, class, module, package or session
* You can teardown fixutres as well by using yield instead, when the fixture is complete it will execture the code after the yield
* Fixtures can inherit other fixtures

```python
@pytest.fixture
def smtp_connection():
    import smtplib
    return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)

def test_ehlo(smtp_connection):
    response, msg = smtp_connection.ehlo()
    assert response == 250
    assert 0 # for demo purposes

```


##
##### Tempdir
You can use the tmpdir fixture which will provide a temporary directory unique to the test invocation, created in the base temporary directory.
Temporary directories are by default created as sub-directories of the system temporary directory. The base name will be pytest-NUM where NUM will be incremented with each test run. Moreover, entries older than 3 temporary directories will be removed.
* You can overide using "pytest --basetemp=mydir"


```python
# content of test_tmpdir.py
import os
def test_create_file(tmpdir):
    p = tmpdir.mkdir("sub").join("hello.txt")
    p.write("content")
    assert p.read() == "content"
    assert len(tmpdir.listdir()) == 1
    assert 0
```

##
##### data_dir
Data Dir is a plugin for pytest that allows you to store test files in directory. pytest-datadir will look up for a directory with the name of your module or the global 'data' folder.

```
.
├── data/
│   └── hello.txt
├── test_hello/
│   └── spam.txt
└── test_hello.py
```



```python
def test_read_global(shared_datadir):
    contents = (shared_datadir / 'hello.txt').read_text()
    assert contents == 'Hello World!\n'

def test_read_module(datadir):
    contents = (datadir / 'spam.txt').read_text()
    assert contents == 'eggs\n'

```


##
##### Homework:
* https://docs.pytest.org/en/latest/contents.html
* https://github.com/gabrielcnr/pytest-datadir

This week we are going to build on homework 6 so if you have not finished it do so. 
* Convert getting the list of missing params into a function
* Convert copy files to a function
* Convert create folder to a function
* Convert processing the command into a function
* If you have done HW5 bonus, make that a function too
* Make a test directory create a py_test file for class 06
* Create a data directory in that folder as well, and put all the logs in it
* Use pytest, os, and tempdir to create tests for creating folders and copying files
* Use pytest and shared_datadir to create a test function for getting a list of files
* Use pytest to test the remaining functions