Python Class 03
=======

# Functions, modules, and packages
Functions are reusable pieces of **function**ality

## Functions
##### Defining a function
```python
def say_hi():
  print('Hey there!') 
```

The function has the name `say_hi` and it prints the string `'Hey There!'`

##### Using a function

```python
say_hi()
```

Using a function is referred to as 'calling' it

You can call it using its name and supply 'parameters' in parentheses


### Functions with parameters

```python
def greet_person(person):
  print('Howdy {}'.format(person))
```

This function is called `greet_person` and it has a parameter called `person`

You can call it like `greet_person('Salazar')` or `greet_person('my friend')`

You can greet a lot of people if you iterate over a list:
```python
people = ['Bataar', 'Chun-hung', 'Quang']
for person in people:
  greet_person(person)
```

We will come back to functions in a bit when we import them from a module

## Modules
### Making a module
Chances are, you have already made your first module!

> A module is a file containing Python definitions and statements.
> 
> -- python.org 2019


Here is a module that defines 3 greeting functions

_greeters.py_
```python
def greet():
  print('Hey')
  
def greet_person(person):
  print('Hi {}'.format(person))

def greet_people(people):
  for person in people:
    greet_person(person)
```

#
###Using a module
If you want to use a module, you have to `import` it first.

Now you can use the functions defined in `greeters.py`

```python
import greeters
greeters.greet_people(['Kwok', 'Alistair', 'Cahill'])
```

Don't want to type 'greeters.' before all the methods you import?
 
Call out the functions you want explicitly

```python
from greeters import greet, greet_person
greet()
greet('darkness, my old friend')
```

Let's try a useful example
```python
import tarfile

filename = 'dump.tar.gz'
tar = tarfile.open(filename, 'r')
tar.extractall()
tar.close()
```

## Packages
Packages are collections of modules in a directory contining a file called `__init__.py`

You do not have to create a package in most cases.

Packages are useful for sharing a collection of reusable functionality and larger programs. 

Modules work fine for this as well. 

## The almighty PIP
### *P*IP *I*nstalls *P*ackages
Let's grab some packages that we can use to interact with the operating system

```
> pip install pywin32
Collecting pywin32
  Downloading https://files.pythonhosted.org/packages/a3/8a/eada1e7990202cd27e58eca2a278c344fef190759bbdc8f8f0eb6abeca9c/pywin32-224-cp37-cp37m-win_amd64.whl (9.0MB)
    100% |████████████████████████████████| 9.1MB 3.8MB/s
Installing collected packages: pywin32
Successfully installed pywin32-224
```

The package name _may_ not (but usually does) match how you 'import' it.

e.g. we will import this using `win32api` and `win32con` instead of `pywin32`

Usually the package will come with some examples to get you started.

### Finding packages
Search for what you want to do on Google. You will _probably_ find a python package to do what you want.

`pip search` exists, but you have to know what you are looking for. 

`pip search [options] <query>`
> Search for PyPI packages whose name or summary contains "query".

## Project time!
### Automatic clicker script

We want to automate mouse interactions on windows.

In this case we will be targeting a 'clicker' game, but your target may be anything.

Be careful though, you don't want to run a clicker forever with no way of stopping it (short of restarting).

### Getting dependencies
Make sure you ran `pip install pywin32` to install it on your machine, otherwise you will get an `ImportError`
```python
import win32api
import win32con
import time
def click(x, y, clicks=1):
    win32api.SetCursorPos((x, y))
    for i in range(0, clicks):
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)
        time.sleep(.1)


print('Place your cursor over the area to be clicked')
time.sleep(5)
x, y = win32api.GetCursorPos()
click(x, y, clicks=100)
```

# Homework
### Music box
![](http://retrowonders.com/ProductImages/1hitwonders_1782_9001376.gif) 

Grab a laptop or a pair of speakers and create a program that periodically beeps

Hint:
```
win32api.Beep
```
Extra credit:

Make a list of frequencies to call `Beep` with

Extra Extra credit:

Make the duration of the beep configurable per note