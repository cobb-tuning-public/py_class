Python Class 8
=======

#####CLASSES and OBECTS
An object combines variables and functions into a single entity. Objects get their variables and functions from classes. Classes are essentially templates for creating your objects. You can think of an object as a single data structure that contains data as well as functions. Functions of objects are called methods, variables are called attributes.

You can initialize a class with default variables.

```python
class MyClass(object):
    var0 = 0
    var1 = 1
    var2 = 2

    def class_function(self):
        return "Hello from inside class_function which is inside MyClass"

my_object = MyClass()
```


##
##### Accessing Variables and Functions
To access variables or functions inside a class use the ".". This might already be familar too you because you have already been interacting with classes in previous lessons. You can also change variables using the same method.
```python
class Car:
    color = ""
    
    def description(self):
        return "This is a {} colored car".format(self.color)

car1 = Car()
car2 = Car()

car1.color = 'Green'

print car1.description()
print car2.description()
car2.color = "Silver"
print car2.description()

```

##
##### Self
The first argument of every class method, including init, is always a reference to the current instance of the class. By convention, this argument is always named self.  In the init method, self refers to the newly created object; in other class methods, it refers to the instance whose method was called.
```python
class Car:
    color = ""
    def description(self):
        return "This is a {} colored car".format(self.color)
    def change_color(self, new_color):
        self.color = new_color

```


##
##### Init
The instantiation operation (“calling” a class object) creates an empty object. Many classes like to create objects with instances customized to a specific initial state. Therefore a class may define a special method named __init__().

Always takes at least one argument, 'self'


```python
class Car:
    def __init__(self, color):
        self.color = color
    def description(self):
        return "This is a {} colored car".format(self.color)
    def change_color(self, new_color):
        self.color = new_color

my_car = Car("Black")
print my_car.description()

```

##
##### Note on being careful with mutable objects
Objects have individuality, and multiple names (in multiple scopes) can be bound to the same object. This is known as aliasing in other languages. This is usually not appreciated on a first glance at Python, and can be safely ignored when dealing with immutable basic types (numbers, strings, tuples). However, aliasing has a possibly surprising effect on the semantics of Python code involving mutable objects such as lists, dictionaries, and most other types. This is usually used to the benefit of the program, since aliases behave like pointers in some respects. For example, passing an object is cheap since only a pointer is passed by the implementation; and if a function modifies an object passed as an argument, the caller will see the change — this eliminates the need for two different argument passing mechanisms as in Pascal.
```python
class Dog:

    tricks = []             # mistaken use of a class variable

    def __init__(self, name):
        self.name = name

    def add_trick(self, trick):
        self.tricks.append(trick)

d = Dog('Fido')
e = Dog('Buddy')
d.add_trick('roll over')
e.add_trick('play dead')
d.tricks     
```


##
##### Class Inheritance
Inheritance is a powerful feature in object oriented programming.

It refers to defining a new class with little or no modification to an existing class. The new class is called derived (or child) class and the one from which it inherits is called the base (or parent) class.
```python
class Polygon(object):
    def __init__(self, num_of_sides):
        self.n = num_of_sides
        self.side_len = [0 for i in range(num_of_sides)]

    def display_side_lengths(self):
        for i in range(self.n):
            log.info("Side {} is {}".format(i+1, self.side_len[i]))

    def find_area(self):
        log.warn("There is no method to calculate area for this shape")


class Triangle(Polygon):
    def __init__(self, side_a, side_b, side_c):
        super(Triangle, self).__init__(3)
        self.side_len[0] = side_a
        self.side_len[1] = side_b
        self.side_len[2] = side_c

    def find_area(self):
        a, b, c = self.side_len
        s = (a + b + c) / 2
        area = (s * (s - a) * (s - b) * (s - c)) ** .5
        log.info("The area of the triangle is {:.2f}".format(area))


class Rectangle(Polygon):
    def __init__(self, width, hieght):
        super(Rectangle, self).__init__(4)
        self.side_len[0] = width
        self.side_len[1] = hieght
        self.side_len[2] = width
        self.side_len[3] = hieght

    def find_area(self):
        a, b, c, d= self.side_len
        log.info("The area of the triangle is {:.2f}".format(a*b))


t = Triangle(5, 6, 7)
t.find_area()
t.display_side_lengths()

log.info(t.side_len)
r = Rectangle(2, 4)
r.find_area()
r.display_side_lengths()

```

##
##### Classes as APIs

You can use classes to abstract functionality to a user, this can be done by wrapping a another module or often a compiled binary.

```python
class VehiclePropsTool(object):
    def __init__(self, exe_location):
        if _is_exe(exe_location):
            self._exe = exe_location
        else:
            raise IOError("Couldn't find {}".format(exe_location))

    def write_vendor(self, v, vehicle=None, vehicle_code=None):
        if vehicle is None and vehicle_code is None:
            log.error("No vehicle name or vehicle code passed to method")
            return False
        exe = self._exe

        args = [
            exe,
            '--vehicle={}'.format(v),
        ]

        if vehicle is not None:
            args.append('--vehicle={}'.format(vehicle))
        if vehicle_code is not None:
            args.append('--vehicle_code={}'.format(vehicle_code))

        log.debug('Running command {}'.format(args))
        proc = subprocess.Popen(
            args=args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_data, stderr_data = proc.communicate()

        if proc.returncode != 0:
            log.error(stdout_data)
            raise RuntimeError('Vehicle modification failed')
        else:
            log.info('Successfully modified: {}'.format(v))
        return True

    def get_pam_properties(self, v):
        exe = self._exe

        args = [
            exe,
            '--vehicle={}'.format(v),
            '-z'
        ]
        log.debug('Running command {}'.format(args))
        proc = subprocess.Popen(
            args=args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_data, stderr_data = proc.communicate()

        if proc.returncode != 0:
            log.error(stdout_data)
            raise RuntimeError('Could Not get Vehicle properties')
        props = {}
        for line in stdout_data.splitlines():
            if "=" in line:
                lines = line.split('=')
                props[lines[0]]=lines[1]
        return props
```


##
##### Homework:
Use the tool class below to create a child class that works on a "MapPropsTool", it should read out a Map's properties and store them in an class instance level dictionary. Create methods to write each property into a map as well, also create a method the get a value for a key from the class's internal dictiory, return False if that key does not exist. Finally make sure to write tests for this module and don't forget to use Logging!
```python
import os

class Tool(object):
     def __init__(self, exe_location):
        if self._is_exe(exe_location):
            self._exe = exe_location
        else:
            raise IOError("Couldn't find {}".format(exe_location))
     
     @staticmethod
     def _is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
```