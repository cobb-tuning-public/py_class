TOKEN_X = 'x'
TOKEN_O = 'o'


def istoken(c) -> bool:
    return c == TOKEN_X or c == TOKEN_O
    
def isempty(c):
    return not istoken(c)

def other_token(t):
    if t == TOKEN_X:
        return TOKEN_O
    elif t == TOKEN_O:
        return TOKEN_X
    else:
        raise ValueError('must be a token: {}'.format(t))

def canonical_piece(c):
    if c == TOKEN_X or c == TOKEN_O:
        return c
    return ' '