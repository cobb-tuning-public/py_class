import sys
import pygame
import time
import random
import numpy as np
from square import Square, TextBlock
from player import HumanPlayer, ComputerPlayer
from enum import Enum
import csv
from pathlib import Path
from model import TTM
pygame.init()

# Initialize Globals
grid_size = grid_width, grid_height = 800, 800
text_box_offset = 100
screen_size = screen_width, screen_height = grid_width, grid_height + text_box_offset
grid_thickness = 4
grid_row_column_count = 3
white = 255, 255, 255
black = 0, 0, 0
red = 255, 0, 0
blue = 0, 0, 255
round_number = 0
number_of_computer_games=1

class gMode(Enum):
    GAME_MODE_NONE = -1
    GAME_MODE_PVP = 1
    GAME_MODE_PVC = 2
    GAME_MODE_CVC = 3


BASICFONT = pygame.font.SysFont('Arial', 21)


class win(Enum):
    X = 1
    O = -1
    DRAW = 0

class mark(Enum):
    X = 1
    O = -1
    DRAW = 0

# Setup the screen
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("COBB Tic Tac Toe")
# Init Startup Images
cobb_logo = pygame.image.load('COBB.png')
pygame.transform.scale(
    cobb_logo, (int(screen_width / 4), int(screen_height / 4)))

players = [HumanPlayer(), ComputerPlayer()]

buttons = pygame.sprite.Group()


def check_event():
    global square_list
    global player1_turn
    global game_mode
    global round_number
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouseposition = pygame.mouse.get_pos()
            for s in square_list:
                if s.rect.collidepoint(mouseposition) and s.clear and game_mode is not gMode.GAME_MODE_NONE:
                    if player1_turn:
                        round_number += 1
                        s.makeX(round_number)
                    else:
                        s.makeO(round_number)
                    player1_turn = not player1_turn
                    continue
            for button in buttons:
                if button.rect.collidepoint(mouseposition):
                    if button.action != None:
                        button.action()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                reset_game()


def add_text_box(msg="", x=0, y=0, w=0, h=0, action=None):
    button = TextBlock(msg, 0, 0, action=action)
    button.rect.x = x
    button.rect.y = y
    button.rect.w = w
    button.rect.h = h
    buttons.add(button)


def draw_grid(width, height, row_colum_count, line_thickness, color):
    global screen
    half_thick = int(line_thickness / 2)
    pygame.draw.line(screen, color, (0, 0), (0, height),
                     line_thickness)  # Left Line
    pygame.draw.line(screen, color, (width - half_thick, 0),
                     (width - half_thick, height), line_thickness)  # Right Line
    pygame.draw.line(screen, color, (0, 0), (width, 0),
                     line_thickness)  # Top Line
    pygame.draw.line(screen, color, (0, height - half_thick),
                     (width, height - half_thick), line_thickness)  # Bottom Line
    pygame.draw.line(screen, black, (798, 0), (798, 800))

    for i in range(1, row_colum_count):
        pygame.draw.line(screen, color, (int(width / row_colum_count * i), 0),
                         (int(width / row_colum_count * i), height), line_thickness)
        pygame.draw.line(screen, color, (0, int(height / row_colum_count * i)),
                         (width, int(height / row_colum_count * i)), line_thickness)


def create_squares(size, thickness, grid_count):
    grid = pygame.sprite.Group()
    square_size = int((size - (grid_count * thickness)) / grid_count)
    starting_pos = (thickness / 2, thickness / 2)
    something = [[0 for x in range(grid_row_column_count)]
                 for y in range(grid_row_column_count)]

    for row in range(1, grid_count + 1):
        for col in range(0, grid_count):
            x_pos = (col * square_size) + starting_pos[0] + (col * thickness)
            item = Square(square_size, x_pos, starting_pos[1])
            grid.add(item)
            something[row - 1][col] = item
        starting_pos = (thickness / 2, (square_size + thickness)
                        * row + thickness / 2)
    return grid

def choose_neural_square(turn):
    global square_list
    # [0, 0, 1, 0, 0, 0, 0, 0, 0] O made first move
    # Make a list of possible X moves 
    # [-1, 0, 1, 0, 0, 0, 0, 0, 0]
    # [0, -1, 1, 0, 0, 0, 0, 0, 0]
    # [0, 0, 1, -1, 0, 0, 0, 0, 0]
    # [0, 0, 1, 0, -1, 0, 0, 0, 0]
    # [0, 0, 1, 0, 0, -1, 0, 0, 0]
    # Find out what squares are availible 
    # Make a list of lists of turns to feed into the prediction algo
    
    curr_game_state = [win.DRAW.value if s.clear else win.X.value if s.x else win.O.value for s in square_list]
    possible_moves = []
    for n, item in enumerate(curr_game_state):
        if curr_game_state[n] == win.DRAW.value:
            possible_move = curr_game_state.copy()
            possible_move[n] = turn.value
            possible_moves.append(possible_move)
    print(possible_moves)
    return


def choose_random_square():
    global square_list
    choose_neural_square(win.X)
    return random.choice(list(filter(lambda n: n.clear is True, square_list)))


def text_objects(text, font, color=black):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


def board_full():
    global square_list
    return True if len(list(filter(lambda n: n.clear is True, square_list))) == 0 else False



# event loop:
#   assume mousedown & pos = position
#   for button in buttons:
#       if (perform hittest):
#           button.action(buttons)
# def add_button():
#   buttons.add(TextBlock('Hey there!', (some_position), action=say_hello))
#   buttons.clear()

def pvp_set_mode():
    global game_mode
    pygame.display.set_caption("Player Vs Player")
    game_mode = gMode.GAME_MODE_PVP
    init_game(game_mode)


def pvc_set_mode():
    global game_mode
    pygame.display.set_caption("Player Vs Computer")
    game_mode = gMode.GAME_MODE_PVC
    init_game(game_mode)


def cvc_set_mode():
    global game_mode
    global comp_turn
    global round_number
    pygame.display.set_caption("Computer Vs Computer")
    game_mode = gMode.GAME_MODE_CVC
    results = []
    wins = [0, 0, 0]
    for i in range(0, number_of_computer_games):
        comp_turn = True
        round_number = 0
        results.extend(comp_vs_comp())
        wins[winner.value] += 1
        clear_board()
    write_game_results(results)
    print(wins)
    reset_game()


def clear_board():
    for s in square_list:
        s.clearSquare()


# Winner 1 (1 X, 0 O, -1 Draw)
# X placement [1 1 1 0 0 0 0 0 0]
# O Placement [0 0 0 1 1 0 0 0 0]
# Blanks      [0 0 0 0 0 1 1 1 1]
comp_turn = True


def comp_vs_comp():
    global winner
    global comp_turn
    global round_number

    if check_win('x'):
        winner = win.X
        return get_game_state()
    if check_win('o'):
        winner = win.O
        return get_game_state()
    if board_full():
        winner = win.DRAW
        return get_game_state()
    if comp_turn:
        round_number += 1
        choose_random_square().makeX(round_number)
    else:
        choose_random_square().makeO(round_number)

    comp_turn = not comp_turn

    return comp_vs_comp()


def reset_game():
    global screen
    global square_list
    global game_mode
    global round_number
    game_mode = gMode.GAME_MODE_NONE
    screen.fill(white)
    screen.blit(cobb_logo, (int(screen_width / 8), int(screen_height / 4)))
    pygame.display.update()
    time.sleep(1)
    screen.fill(white)
    draw_grid(grid_width, grid_height,
              grid_row_column_count, grid_thickness, red)
    clear_board()
    round_number = 0
    pygame.display.update()
    pygame.display.set_caption("Menu")
    add_text_box("Player Vs Player", 50, 850, 200, 50, pvp_set_mode)
    add_text_box("Player Vs Computer", 550, 850, 200, 50, pvc_set_mode)
    add_text_box("Computer Vs Computer", 275, 850, 200, 50, cvc_set_mode)


def init_game(mode):
    # mode => True = pvp, False = pvc
    global screen
    global square_list
    global player1_turn
    player1_turn = True
    screen.fill(white)
    screen.fill(white)
    draw_grid(grid_width, grid_height,
              grid_row_column_count, grid_thickness, red)
    for s in square_list:
        s.clearSquare()
    pygame.display.update()


def update_game():
    square_list.update()
    square_list.draw(screen)
    buttons.update()
    buttons.draw(screen)
    pygame.display.update()


# Create Grid
square_list = create_squares(grid_width, grid_thickness, grid_row_column_count)
# Init the screen
reset_game()
player1_turn = True
game_mode = gMode.GAME_MODE_NONE
winner = win.DRAW

# [ (0), (1), (2), (3), (...), (), (), (), () ]


def check_win(attribute):
    global grid_row_column_count
    global square_list
    assert(grid_row_column_count > 0)
    assert(hasattr(square_list.sprites()[0], attribute))
    check_array = np.asarray(square_list.sprites()).reshape(
        grid_row_column_count, grid_row_column_count)

    def check_iterator(it):
        for i in it:
            if len(list(filter(lambda n: n.__dict__[attribute] is True, i))) == grid_row_column_count:
                return True
        return False

    def check_horizontal():
        return check_iterator(check_array)

    def check_vertical():
        return check_iterator(check_array.T)

    def check_diagonal():
        d1 = list()
        d2 = list()
        for r, u, d in zip(check_array, range(0, grid_row_column_count), range(grid_row_column_count - 1, -1, -1)):
            d1.append(r[u])
            d2.append(r[d])

        return check_iterator([d1, d2])

    return check_horizontal() or check_vertical() or check_diagonal()


def should_display_game_end():
    # Only show the end screen stuff if there is a player
    global game_mode
    return game_mode != gMode.GAME_MODE_CVC


def write_game_results(results, file='ttt.csv'):
    fpath = Path(file)
    write_header=False
    if not fpath.exists():
        write_header=True

    with open('ttt.csv', 'a', newline='') as csvfile:
        fieldnames = ['Winner', 'Round', 'Turn']
        # fieldnames = ['Winner', 'Round', 'X State', 'O State', 'Clear State','Turn']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if write_header:
            writer.writeheader()
        writer.writerows(results)


def get_game_state():
    global square_list
    global winner
    global round_number
    x_hot_list = [s.turn if s.x else 0 for s in square_list]
    o_hot_list = [s.turn if s.o else 0 for s in square_list]
    blank_hot_list = [1 if s.clear else 0 for s in square_list]
    turn_list_list = []
    turn_list = [0] * 9

    for i in range(1, 10):
        for n, square in enumerate(square_list): # Loops squares
            if square.turn == i and square.x:
                turn_list[n] = mark.X.value
                turn_list_list.append({'Winner': winner.value, 'Round': round_number, 'Turn': turn_list.copy()})
        for n, square in enumerate(square_list): # Loops squares
            if square.turn == i and square.o:
                turn_list[n] = mark.O.value
                turn_list_list.append({'Winner': winner.value, 'Round': round_number, 'Turn': turn_list.copy()})
    # Winner 1 (1 X, 0 O, -1 Draw)
    # [0 , 1 ,2
    #  3, 4, 5,
    #  6, 7, 8]
    #[ s.turn else 0 for s in ss]
    # Cols, Winner, Round, X State, O State, Clear State

    return turn_list_list
    # return {'Winner': winner.value, 'Round':round_number, 'X State': x_hot_list, 'O State': o_hot_list, 'Clear State': blank_hot_list}

    # X placement [1 2 3 0 0 0 0 0 0]
    # O Placement [0 0 0 1 2 0 0 0 0]
    # Blanks      [0 0 0 0 0 1 1 1 1]
    # enumerate square list


def someone_wins(player):
    if should_display_game_end():
        win_text = text_objects(player + ' wins!!!!!!!!!1', BASICFONT)
        screen.fill(white)
        screen.blit(win_text[0], (int(screen_width / 8),
                                  int(screen_height / 4)))
        pygame.display.update()
        time.sleep(1)
    reset_game()


def check_and_handle_game_end():
    if check_win('x'):
        someone_wins('x')
    if check_win('o'):
        someone_wins('o')
    if board_full():
        someone_wins('Draw')

mainModel = TTM(9, 3, 64, 16)
model_csv = Path('D:\\dev\\py_class\\ml\\ttt\\ttt_smol.csv')
# mainModel.train(str(model_csv))
# Main Loop
while(True):
    check_event()
    update_game()
    if game_mode != gMode.GAME_MODE_NONE:
        if not player1_turn and game_mode == gMode.GAME_MODE_PVC and not board_full():
            choose_sqaure().makeO()
            player1_turn = True
        check_and_handle_game_end()


# Create new game mode computer vs computer
# We want to write the game state and board state to a file
# Get stats on CvC random plays
# Winner 1 (1 X, 0 O, -1 Draw)
# X placement [1 1 1 0 0 0 0 0 0]
# O Placement [0 0 0 1 1 0 0 0 0]
# Blanks      [0 0 0 0 0 1 1 1 1]
# win conditionn

# win conditions
# (0, 1, 2)
# (0, 3, 6)
# (0, 4, 9)
# ...
# x-squares (3, 4 ,5) -> % 3 -> (0, 1, 2)

# ( x, , )
# ( , x, )
# ( , , x)

# What we're going to pass into our ML model????
# [ '', '', '', 'o', 'x', 'o', '', '', '']

# What we get back??
