from tensorflow.keras.layers import Dense, Input, DenseFeatures
from tensorflow.keras.layers import Dropout
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical, get_file
from tensorflow.feature_column import numeric_column, sequence_numeric_column
import tensorflow as tf
from pathlib import Path
import pandas
import numpy as np
import ast


class TTM:
    def __init__(self, number_of_inputs, number_of_outputs, epochs, batch_size):
        self.epochs = epochs
        self.batch_size = batch_size
        self.number_of_inputs = number_of_inputs
        self.number_of_outputs = number_of_outputs
        self.model = Sequential()
        input_shape = (number_of_inputs, )
        self.model.add(Dense(64, activation='relu', input_shape=input_shape))
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dense(number_of_outputs, activation='softmax'))
        self.model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    def train(self, data_csv, training_ratio=.8):
        # Get the data frame
        try:
            data_frame = pandas.read_csv(data_csv, usecols=['Winner', 'Round', 'Turn'])

        except IOError:
            print('Could not open file...')
            return False
        output_state = to_categorical(data_frame['Winner'].to_list(), num_classes=3)
        output_train, output_val = np.split(output_state, [int(len(output_state)*0.8)])
        round_state = to_categorical(data_frame['Round'].to_list(), num_classes=6)
        round_train, round_val = np.split(round_state, [int(len(round_state)*0.8)])
        turn_state = list(data_frame.Turn.apply(lambda x:ast.literal_eval(str(x))))
        turn_state = pandas.DataFrame(turn_state, columns=["0","1","2","3","4","5","6","7","8"])
        turn_train, turn_val = np.split(turn_state, [int(len(turn_state)*0.8)])
        self.model.fit(turn_train, output_train, validation_data=(turn_val, output_val), epochs=self.epochs, batch_size=self.batch_size)
        return True

    def predict(self, data, index):
        prd = self.model.predict([data])
        # print(prd)
        return prd[0][index]


if __name__ == "__main__":
    model = TTM(9, 3, 64, 16)
    csv = Path('D:\\dev\\py_class\\ml\\ttt\\ttt.csv')
    print(csv.exists())

# [0. 0. 1.] X WIN
# [0. 1. 0.] Y WIN
# [1. 0. 0.] DRAW

    XWIN = 2
    YWIN = 1
    DRAW = 0
    if not model.train(str(csv)):
        print('Training Failed')
    td = [  [-1, 0, 0, 0, 0, 0, 0, 1, -1],
            [0, -1, 0, 0, 0, 0, 0, 1, -1],
            [0, 0, -1, 0, 0, 0, 0, 1, -1],
            [0, 0, 0, -1, 0, 0, 0, 1, -1],
            [0, 0, 0, 0, -1, 0, 0, 1, -1],
            [0, 0, 0, 0, 0, -1, 0, 1, -1],
            [0, 0, 0, 0, 0, 0, -1, 1, -1]]
    tb = [  [0, 0, 0, 0, 0, 0, 0, 0, -1],
            [0, 0, 0, 0, 0, 0, 0, -1, 0],
            [0, 0, 0, 0, 0, 0, -1, 0, 0],
            [0, 0, 0, 0, 0, -1, 0, 0, 0],
            [0, 0, 0, 0, -1, 0, 0, 0, 0],
            [0, 0, 0, -1, 0, 0, 0, 0, 0],
            [0, 0, -1, 0, 0, 0, 0, 0, 0],
            [0, -1, 0, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0, 0, 0]]
    predicts = []
    for n,t in enumerate(tb):
        predicts.append(model.predict(t, XWIN))
    print(predicts)
    print(f'Chance of X {max(predicts)}')
    
