from tokens import TOKEN_X, TOKEN_O


class Player(object):
    '''
    A player places tokens
    '''
    def __init__(self, token, human):
        self.token = token
        self.human = human

    def place_piece(row, column, board):
        # valid_move = board.place(self.token, row, column)
        pass


class HumanPlayer(Player):
    def __init__(self):
        super(HumanPlayer, self).__init__(True, TOKEN_X)


class ComputerPlayer(Player):
    def __init__(self):
        super(ComputerPlayer, self).__init__(False, TOKEN_O)

    def place_piece(row, column, board):
        # Evaluate the board and pick a winning move!
        # 01001011010010010100110001001100 010000010100110001001100
        # 01001000010101010100110101000001010011100101001100100001
        pass








