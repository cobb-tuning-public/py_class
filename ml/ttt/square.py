import pygame


class Square(pygame.sprite.Sprite):
    """This is going to be a square, so width == height
    """

    def __init__(self, size, x, y):
        """[summary]


        Args:
            image (str): What we should look like!
            size (int): How big we should be.
            x (int): starting coordinates!
            y (int): starting coordinates!
        """
        pygame.sprite.Sprite.__init__(self)

        self.size = size
        self.clearSquare()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.turn = 0


    def makeX(self, turnNumber = 0):
        if not self.clear:
            return
        self.x = True
        self.o = False
        self.clear = False
        self.setImage('x.png')
        self.turn = turnNumber
        

    def makeO(self, turnNumber=0):
        if not self.clear:
            return False
        self.o = True
        self.clear = False
        self.setImage('o.png')
        self.turn = turnNumber


    def clearSquare(self):
        self.x = False
        self.o = False
        self.clear = True
        self.clearImage()
        self.turn = 0


    def setImage(self, imagePath):
        self.image = pygame.image.load(imagePath)
        self.image = pygame.transform.scale(self.image, (self.size, self.size))


    def clearImage(self):
        self.image = pygame.Surface([self.size, self.size])
        self.image.fill((255, 255, 255))


    def makeRobot(self):
        self.setImage('robot_alpha.png')
        return True


class TextBlock(pygame.sprite.Sprite):
    """This is going to be a textblock
    """

    def __init__(self, text, x, y, **kwargs):
        """[summary]


        Args:
            text (str): what do say!
            size (int): How big we should be.
            x (int): starting coordinates!
            y (int): starting coordinates!
        """
        pygame.sprite.Sprite.__init__(self)
        font = pygame.font.SysFont(None, 32)
        self.text = text
        self.image = font.render(
            self.text, True, (255, 255, 255), (100, 100, 100))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.action = kwargs['action']
