import numpy as np

# one_dimensional_array = np.array([1.2, 2.4, 3.5, 4.7, 6.1, 7.2, 8.3, 9.5])
# print(one_dimensional_array)

# two_dimensional_array = np.array([[6, 5], [11, 7], [4, 8]])
# print(two_dimensional_array)

# sequence_of_integers = np.arange(5, 12)
# print(sequence_of_integers)

# random_integers_between_50_and_100 = np.random.randint(low=50, high=101, size=(6))
# print(random_integers_between_50_and_100)

random_floats_between_0_and_1 = np.random.random([6])
print(random_floats_between_0_and_1)

random_floats_between_2_and_3 = random_floats_between_0_and_1 + 2.0
print(random_floats_between_2_and_3)


'''
Assign a sequence of integers from 6 to 20 (inclusive) to a NumPy array named feature.
Assign 15 values to a NumPy array named label such that:
label = (3)(feature) + 4
The first label should be:
label = (3)(6) + 4 = 22
Code template:

feature = ? # write your code here
print(feature)
label = ?   # write your code here
print(label)
'''
feature = np.arange(6, 21)
print(feature)
label = (feature * 3) + 4
print(label)
feature2 = feature.copy()
feature += 2

print(feature)
print(feature2)
'''
To make your dataset a little more realistic, insert a little random noise into each element of the label array you already created.
To be more precise, modify each value assigned to label by adding a different random floating-point value between -2 and +2.

Don't rely on broadcasting. Instead, create a noise array having the same dimension as label.
'''
