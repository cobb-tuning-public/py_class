import pygame


class Player(pygame.sprite.Sprite):
    def __init__(self, image, width=50, height=50):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.image.load(image)
        self.image = pygame.transform.scale(self.image, (width, height))
        self.image = self.image.convert_alpha()
        self.rect = self.image.get_rect()
        self.dx = 0
        self.dy = 0

    def update(self):
        pass
