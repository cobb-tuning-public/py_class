import sys
import pygame
import time
from random import randint
from pygame import Surface, Rect, SRCALPHA
from block import Block
from pathlib import Path
from zergling import Zergling
pygame.init()


high_score = 0.0
size = width, height = 800, 600
speed = [0, 0]
left_right = 0
up_down = 1
grey = 140, 140, 140
red = 255, 0, 0
cyan = 0, 174, 239
green = 0, 255, 0
yellow = 255, 255, 0
zergie_is_dead = False


p = Path('save.txt')
# YY U FLOAT?
if p.exists():
    with p.open('r') as f: high_score = float(f.readline())


print(high_score)
bhigh_score = False

def updateLand():
    global landLevel, landChange, roofLevel, roofChange
    if randint(0, 10) == 3:
        roofChange = randint(0, 6) - 3
    if randint(0, 10) == 3:
        landChange = randint(0, 6) - 3
    roofLevel += roofChange
    landLevel += landChange
    landLevel = limit(landLevel, 200, 590)
    roofLevel = limit(roofLevel, 10, 400)
    if roofLevel > landLevel - 200:
        roofLevel = landLevel - 200
    scrambleSurface.scroll(-1, 0)
    drawLand()


def limit(n, minn, maxn):
    return max(min(maxn, n), minn)


def drawLand():
    for i in range(0, 600):
        c = (0, 0, 0, 0)
        if i > landLevel:
            g = limit(i - landLevel, 0, 255)
            c = (255, g, 0)
        elif i < roofLevel:
            r = limit(roofLevel - i, 0, 255)
            c = (255, r, 0)
        scrambleSurface.set_at((799, i), c)


def checkMovement():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_DOWN:
                zergie2.velocity_y += 1
                speed[up_down] = speed[up_down] + 1
            elif event.key == pygame.K_UP:
                zergie2.velocity_y -= 1
                speed[up_down] = speed[up_down] - 1
            elif event.key == pygame.K_LEFT:
                zergie2.velocity_x -= 1
                speed[left_right] = speed[left_right] - 1
            elif event.key == pygame.K_RIGHT:
                zergie2.velocity_x += 1
                speed[left_right] = speed[left_right] + 1


def collision() -> bool:
    color_list = [
        scrambleSurface.get_at(zergie_rect.midtop),
        scrambleSurface.get_at(zergie_rect.midleft),
        scrambleSurface.get_at(zergie_rect.midbottom),
        scrambleSurface.get_at(zergie_rect.midright)
    ]
    for color in color_list:
        if color[3] != 0:
            return True
    return False


def boundCheck():
    leftloc = zergie_rect.midleft[0]
    rightloc = zergie_rect.midright[0]
    toploc = zergie_rect.midtop[1]
    downloc = zergie_rect.midbottom[1]
    # Movement handling
    if leftloc <= 0:
        speed[left_right] = speed[left_right] * -1
        zergie_rect.x = 0
    if rightloc >= width:
        # Reposition this object to start on the left hand side
        zergie_rect.x = 0
    if toploc <= 0:
        speed[up_down] = speed[up_down] * -1
        zergie_rect.y = 0
    if downloc >= height:
        speed[up_down] = speed[up_down] * -1
        zergie_rect.y = height - (zergie_rect.height) - 1


sass = pygame.font.SysFont('freesansbold.tff', 25).render("YOU JUST GOT SASSED!", 1, red)
dead = pygame.font.SysFont('comicsans.tff', 25).render("DED DED DED DED !", 1, red)
score_font = pygame.font.SysFont('comicsans.tff', 50)

screen = pygame.display.set_mode(size)
zergie = pygame.image.load("zergie.png")
zergie = pygame.transform.scale(zergie, (100, 50))
zergie_rect = zergie.get_rect()

explode = pygame.image.load("explode.png")
explode = pygame.transform.scale(explode, (100, 100))
explode_rect = explode.get_rect()

spawn_noise = pygame.mixer.Sound("spawn.wav")
spawn_noise.play()
death_noise = pygame.mixer.Sound("explode.wav")


scrambleSurface = Surface(size, SRCALPHA)
landLevel = 600
roofLevel = 10
landChange = -3
roofChange = 3

zergie_is_dead = False
current_color = grey
all_sprites_list = pygame.sprite.Group()
zergie2 = Zergling("zergie.png", 100, 50)
zergie2.rect.x = 10
zergie2.rect.y = 10
all_sprites_list.add(zergie2)
clock = pygame.time.Clock()
start_time = clock.get_time()

while 1:
    checkMovement()
    zergie_rect = zergie_rect.move(speed)

    screen.fill(current_color)
    boundCheck()
    updateLand()
    #zergie_is_dead = collision()
    #zergie2.dead = zergie_is_dead

    #screen.blit(zergie, zergie_rect)
    screen.blit(scrambleSurface, (0, 0))
    all_sprites_list.update()
    all_sprites_list.draw(screen)

    backgroundmask = pygame.mask.from_threshold(scrambleSurface, (0,0,0,0), (255,127,255,255))
    backgroundmask.invert()
    
    maskdebug = False
    if maskdebug == True:
        screen.blit(backgroundmask.to_surface(setcolor=(255,10,10,255), unsetcolor=(0,255,20,200)), scrambleSurface.get_rect())
        screen.blit(zergie2.mask.to_surface(setcolor=(255,10,10,255), unsetcolor=(0,255,20,20)), zergie2.rect)
    elapsed_time = pygame.time.get_ticks() / 1000
    
    overlap = zergie2.mask.overlap(backgroundmask, (-zergie2.rect.x, -zergie2.rect.y))
    if overlap is not None:
        zergie_is_dead = True
    else:
        zergie_is_dead = False
    
    screen.blit(score_font.render(f'{elapsed_time}', 1, (0,0,0)), (10, 500))
    if zergie_is_dead:
        # Stop the game
        death_noise.play()
        if (elapsed_time > high_score):
            with open('save.txt', 'w') as f:
                f.write(str(elapsed_time))
                bhigh_score = True
                high_score = elapsed_time
        screen.blit(explode, zergie2.rect)
        break
    else:
        pass
    pygame.display.flip()
    clock.tick(60)

while True:
    # Show them the endgame
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
    if bhigh_score:
        msg = score_font.render(f'HI SCORE, BOOMER! {high_score}', 1, (0, 0, 0), red)
        # high_score_msg
    else:
        msg = score_font.render(f'OKLAHOMA BOOMER', 1, (0, 0, 0), cyan)
    
    craziness = 6
    offset_x, offset_y = randint(-craziness, craziness), randint(-craziness, craziness)
    screen.blit(msg, (offset_x, height/2 + offset_y))

    pygame.display.flip()
    clock.tick(10)