import pygame

class Zergling(pygame.sprite.Sprite):
    velocity_x_max = 30
    velocity_y_max = 30

    def __init__(self, image, width, height):
       pygame.sprite.Sprite.__init__(self)
       self.image = pygame.image.load(image).convert_alpha()
       self.image = pygame.transform.scale(self.image, (width, height))
       self.rect = self.image.get_rect()
       self.dead = False
       self.velocity_x = 0
       self.velocity_y = 0

    def is_dead(self):
        return self.dead

    def update(self):
        # Limit your velocity
        self.velocity_x = min(self.velocity_x, Zergling.velocity_x_max)
        self.velocity_y = min(self.velocity_y, Zergling.velocity_y_max)

        self.rect.x += self.velocity_x
        self.rect.y += self.velocity_y
        self.mask = pygame.mask.from_surface(self.image)
