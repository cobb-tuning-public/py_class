# py_class #

### COBB Python Training Course ###

* This repo contains a series of course notes and sample code from each lesson

### Setup ###

* The course will use both python 2.7 and 3.7
* The IDE used will be PyCharm

## Overview

### Class 1
Intro & getting set up

### Class 2
Conditionals and keywords, FizzBuzz homework

### Class 3
User-defined functions, modules, & packages, cookie clicker homework