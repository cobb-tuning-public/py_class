import os
import csv
import logging
from random import random, randrange

log = logging.getLogger(__name__)

import pygame
from pygame import *

"""Put your game's helper classes here"""
from camera import Camera
from entities import Player, Platform

"""Global game defines (think configuration)"""
WIN_WIDTH = 800
WIN_HEIGHT = 640
HALF_WIDTH = int(WIN_WIDTH / 2)
HALF_HEIGHT = int(WIN_HEIGHT / 2)

DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
DEPTH = 32
FLAGS = 0
car_image = pygame.image.load(r'assests\images\trans_prowler_scaled_r.png')

platform = Platform((HALF_WIDTH, HALF_HEIGHT + 100))
platform_sprites = pygame.sprite.RenderPlain(platform)
wrx = Player([platform], (HALF_WIDTH, HALF_HEIGHT), image=car_image)
wrx_sprite = pygame.sprite.RenderPlain(wrx)

entities = pygame.sprite.Group()
entities.add(wrx_sprite, platform_sprites)

def main():
    """
    Start up the screen and pygame. Get ourselves all set to draw things
    """
    
    pygame.init()
    pygame.mixer.init()
    intro_sound = pygame.mixer.Sound(r'assests\sound\car+start3.wav')
    # vroom_sound = pygame.mixer.Sound(r'assests\sound\CIVIC+passing+at+high+speed.mp3')
    screen = pygame.display.set_mode(DISPLAY, FLAGS, DEPTH)
    pygame.display.set_caption("Use arrows to move!")
    timer = pygame.time.Clock()
    
    def will_car_start():
        if randrange(1, 2000, 1) == 2:
            intro_sound.play()
            return True
        else:
            return False

    if not will_car_start():
        wrx.canJump = False

    
    while 1:
        for e in pygame.event.get():
            #K_LEFT
            #K_RIGHT
            #K_SPACE
            #K_DOWN
            if e.type == QUIT: 
                return
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    pass
                if e.key == K_LEFT:
                    pass
                if e.key == K_RIGHT:
                    pass
                if e.key == K_DOWN:
                    pass
                if e.key == K_SPACE:
                    pass
        # gloals[5:100]
        screen.fill(Color('gray'))

        entities.update()
        entities.draw(screen)

        pygame.display.update()
        timer.tick(60)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)2s: %(name)2s: %(funcName)2s: %(message)s')
    main()
