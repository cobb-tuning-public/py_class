import pygame


class Obstacle(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        super().__init__()

        self.image = Surface([width, height])
        self.image.fill(Color('black'))
        self.image.set_colorkey(Color('black'))

        pygame.draw.rect(self.image, color, [0, 0, width, height])

        self.rect = self.image.get_rect()
