import pytest
from src.py_class_08 import *

def test_MyClass():
    obj = MyClass()
    assert obj.var1 == 1
    assert obj.var2 == 2
    assert obj.class_function() == "Hello from inside class_function which is inside MyClass"