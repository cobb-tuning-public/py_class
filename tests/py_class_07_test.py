import pytest
from src.py_class_07 import *

def test_inc():
    assert inc(3) == 4


class TestParseCSV():

    @pytest.fixture()
    def fb_data(self , shared_datadir):
        yield read_data(str(shared_datadir / 'football.csv'))

    @pytest.fixture
    def w_data(self, shared_datadir):
        return read_data(str(shared_datadir / 'weather.csv'))

    def test_csv_read_football_headers(self, fb_data):
        for x in fb_data[0].keys():
            assert x in ['Team', 'Games', 'Wins', 'Losses', 'Draws', 'Goals', 'Goals Allowed', 'Points']

    def test_csv_read_data_team_name(self, fb_data):
        assert fb_data[0].get('Team') == 'Arsenal'

    def test_csv_read_data_point(self, fb_data):
        assert fb_data[0].get('Points') == '87'

    def test_get_min_difference(self, fb_data):
        assert fb_data[get_min_difference(fb_data)].get('Team') == 'Aston_Villa'

    def test_csv_read_weather_headers(self, w_data):
        for x in w_data[0].keys():
            assert x in ['Day','MxT','MnT','AvT','AvDP','1HrP TPcpn','PDir','AvSp','Dir','MxS','SkyC','MxR','Mn','R AvSLP']
