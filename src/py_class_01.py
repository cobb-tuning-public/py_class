# String
hello_var = "Hello World"
print(hello_var)
print (type(hello_var))

# Integer
hello_var = 5
print(hello_var)
print (type(hello_var))

# Float
hello_var = 5.5
print(hello_var)
print (type(hello_var))

# Boolean
hello_var = True
print(hello_var)
print (type(hello_var))
hello_var = False
print(hello_var)
print (type(hello_var))

# None
hello_var = None
print(hello_var)
print (type(hello_var))

# Tuple
hello_var = 1, 'Taco', False
print (hello_var[1])
print (type(hello_var))

# List
hello_var = ["Hello World", 5, True, None]
print(hello_var)
print (type(hello_var))

# Dict
hello_var ={'Taco':5, 'Burrito':2, 'Quesadilla':20}
print(hello_var)
print (type(hello_var))