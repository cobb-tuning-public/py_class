######################## FUNCTIONS ########################
# No parameters, no return value
def say_hi():
    print('Hey there!')


say_hi()


# One parameter, no return value
def greet_person(person):
    print('Howdy {}'.format(person))


people = ['Bataar', 'Chun-hung', 'Quang']
for person in people:
    greet_person(person)


# One parameter, no return value
def greet_people(people):
    for person in people:
        greet_person(person)


greet_people(people)


# Default arguments
def greet_default(person='Buddy'):
    print('Hi {}'.format(person))


greet_default()
greet_default('Hermes')

######################## MODULES AND IMPORTS ########################
import glob
"""
The glob module finds all the pathnames matching a specified pattern according 
to the rules used by the Unix shell, although results are returned in arbitrary order.
"""

python_class_files = glob.glob('*class*.py')
for file in python_class_files:
    print(file)

# Explicit imports let you specify certain things to bring in.
# Note we just use 'item' instead of 'module.item'
from glob import glob
# What do you think this does?
print(glob('../**', recursive=True))