import glob
import os
import csv

default_log_list= ['Engine Speed (RPM)',
                   'Time (sec)',
                   'Knock Retard Cylinder 4 (Degrees)',
                   'Knock Retard Cylinder 3 (Degrees)',
                   'Knock Retard Cylinder 2 (Degrees)',
                   'Knock Retard Cylinder 1 (Degrees)',
                   'Ignition Timing Final (Degrees)',
                   'Trgt. Boost Press. (psi)',
                   'Longitudinal Acceleration (m/s2)']


def get_list_of_files(path, filter):
    # Marvel at this list comprehension
    file_list = [y for x in os.walk(path) for y in glob.glob(os.path.join(x[0], filter))]
    return file_list


def main():
    csv_files = get_list_of_files(os.path.join(os.getcwd(),'py_class_05'),'*.csv')
    for file in csv_files:
        try:
            with open(file) as csvfile:
                cf = csv.DictReader(csvfile)
                ''' This is where you would want to do your work, perhaps a function...'''
        except IOError as e:
            print (e)
            exit()


if __name__ == '__main__':
    main()