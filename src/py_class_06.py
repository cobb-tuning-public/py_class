import os
import csv
import logging
from py_class_06_0 import get_list_of_files, get_missing_params
log = logging.getLogger(__name__)

knock_retards = ['Knock Retard Cylinder 4 (Degrees)',
                   'Knock Retard Cylinder 3 (Degrees)',
                   'Knock Retard Cylinder 2 (Degrees)',
                   'Knock Retard Cylinder 1 (Degrees)',]

log_list = ['Engine Speed (RPM)',
                   'Time (sec)',
                   'Ignition Timing Final (Degrees)',
                   'Trgt. Boost Press. (psi)',
                   'Longitudinal Acceleration (m/s2)',
                   ]
default_log_list = log_list + knock_retards


def main():
    csv_files = get_list_of_files(os.path.join(os.getcwd(), 'py_class_05'), '*.csv')
    for file in csv_files:
        try:
            with open(file) as csvfile:
                log.debug('Checking file : {}'.format(file))
                cf = csv.DictReader(csvfile)
                log.info(cf.fieldnames)
                get_missing_params(cf.fieldnames, default_log_list)

        except IOError as e:
            log.error(e)
            exit()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)2s: %(name)2s: %(funcName)2s: %(message)s')
    log.info('Running py_class_06')
    main()

import logging
import sys

log = logging.getLogger(__name__)
lf = 'py.log'
fh = logging.FileHandler(lf, mode='w')
format = logging.Formatter('%(levelname)2s: %(name)2s: %(funcName)2s: %(message)s')
fh.setFormatter(format)
log.addHandler(fh)
stdh = logging.StreamHandler(sys.stdout)
stdh.setFormatter(format)
stdh.setLevel(logging.WARNING)
log.addHandler(stdh)
log.setLevel(logging.DEBUG)

log.debug('Test Debug Out')
log.warn('This is a Drill')

import logging
import subprocess
import os
log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

command= ['cpasdfasd']
try:
    proc = subprocess.Popen(args=command, cwd=os.getcwd(),
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_data, stderr_data = proc.communicate()
    if proc.returncode != 0:
        log.info(proc.returncode)
        log.info(stdout_data)
        log.error(stderr_data)
        exit()
    log.info(stdout_data)
except OSError as e:
    log.error(e)

