import logging
log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(levelname)2s: %(name)2s: %(funcName)2s: %(message)s')


class MyClass(object):
    var0 = 0
    var1 = 1
    var2 = 2

    def class_function(self):
        return "Hello from inside class_function which is inside MyClass"


my_object_0 = MyClass()
my_object_1 = MyClass()

log.info(my_object_0.var1)
log.info(my_object_1.class_function())
log.info(my_object_0.class_function())

my_object_0.var1 = 5

log.info(my_object_0.var1)
log.info(my_object_1.var1)

class Car:
    color = ""

    def description(self):
        return "This is a {} colored car".format(self.color)


car1 = Car()
car2 = Car()

car1.color = 'Green'

log.info(car1.description())
log.info(car2.description())
car2.color = "Silver"
log.info(car2.description())


class Car:
    def __init__(self, color):
        self.color = color

    def description(self):
        return "This is a {} colored car".format(self.color)

    def change_color(self, new_color):
        self.color = new_color


my_car = Car("Black")
log.info(my_car.description())


class Dog:

    tricks = []             # mistaken use of a class variable

    def __init__(self, name):
        self.name = name

    def add_trick(self, trick):
        self.tricks.append(trick)

d = Dog('Fido')
e = Dog('Buddy')
d.add_trick('roll over')
e.add_trick('play dead')
log.info(d.tricks)


class Polygon(object):
    def __init__(self, num_of_sides):
        self.n = num_of_sides
        self.side_len = [0 for i in range(num_of_sides)]

    def display_side_lengths(self):
        for i in range(self.n):
            log.info("Side {} is {}".format(i+1, self.side_len[i]))

    def find_area(self):
        log.warn("There is no method to calculate area for this shape")


class Triangle(Polygon):
    def __init__(self, side_a, side_b, side_c):
        super(Triangle, self).__init__(3)
        self.side_len[0] = side_a
        self.side_len[1] = side_b
        self.side_len[2] = side_c

    def find_area(self):
        a, b, c = self.side_len
        s = (a + b + c) / 2
        area = (s * (s - a) * (s - b) * (s - c)) ** .5
        log.info("The area of the triangle is {:.2f}".format(area))


class Rectangle(Polygon):
    def __init__(self, width, hieght):
        super(Rectangle, self).__init__(4)
        self.side_len[0] = width
        self.side_len[1] = hieght
        self.side_len[2] = width
        self.side_len[3] = hieght

    def find_area(self):
        a, b, c, d = self.side_len
        log.info("The area of the triangle is {:.2f}".format(a*b))


t = Triangle(5, 6, 7)
t.find_area()
t.display_side_lengths()

log.info(t.side_len)
r = Rectangle(2, 4)
r.find_area()
r.display_side_lengths()