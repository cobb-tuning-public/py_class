import csv


def inc(x):
    return x + 1


def read_data(data):
    with open(data, 'r') as f:
        data = csv.DictReader(f)
        return [r for r in data]


def get_min_difference(data):
    goals = [x.get('Goals') for x in data]
    goals_allowed = [x.get('Goals Allowed') for x in data]
    min_score = [float(x)-float(y) for x, y in zip(goals, goals_allowed)]
    return min_score.index(min(min_score))

# print get_min_score(read_data('py_class_07/football.csv'))
