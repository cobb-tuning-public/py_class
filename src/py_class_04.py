# File Open

f = open('py_class_04_countries.txt', 'r')
data = f.readlines()
f.close()

# With Statement

with open('py_class_04_EU.txt', 'r') as f:
    data = f.readlines()

try:
    with open('py_class_04_EU.txt', 'rb') as f:
        data = f.read()
except IOError as e:
    print (e)
    exit()

try:
    with open("py_class_04_countries.txt", "r") as f:
      f.seek(-10, 2)
      data = f.read()
except IOError as e:
    print (e)
    exit()

try:
    with open("py_class_04_countries.txt", "r") as f:
        # Move 10 characters back from the end of the file
        f.seek(-10, 2)
        print (f.tell())
        # Read 1 byte, this will move the offset by two since we are reading as a text file
        print (f.read(5))
        print (f.tell())
        # Seek to 100 bytes from the start of the file
        f.seek(100, 0)
        print (f.tell())
        print (f.read(5))

except IOError as e:
    print (e)
    exit()

planets = ['Mercury\n',
'Venus',
'Earth',
'Mars',
'Jupiter',
'Saturn',
'Uranus',
'Neptune']

planets = ['Mercury\n',
'Venus\n',
'Earth\n',
'Mars\n',
'Jupiter\n',
'Saturn\n',
'Uranus\n',
'Neptune\n']

try:
    with open("py_class_04_output.txt", "w") as f:
        # for planet in planets:
        #     f.(planet)
        f.writelines(planets)

except IOError as e:
    print (e)
    exit()

try:
    with open("py_class_04_EU.txt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            if "United Kingdom" not in line:
                f.write(line)

except IOError as e:
    print (e)
    exit()
