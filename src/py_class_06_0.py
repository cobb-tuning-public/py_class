import glob
import os
import logging

log = logging.getLogger(__name__)

def get_list_of_files(path, filter):
    log.debug(path)
    file_list = [y for x in os.walk(path) for y in glob.glob(os.path.join(x[0], filter))]
    log.debug(file_list)
    return file_list

def get_missing_params(list, master_list):
    log = logging.getLogger(__name__+'.get_missing_params')
    log.setLevel(logging.DEBUG)
    list = [x for x in master_list if x not in list]
    log.debug(list)
    return list

